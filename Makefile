name=example-minted
style_file=eclipse.py
lexer_file=javaelexer.py
pygments_dir=/usr/lib/python3/dist-packages/pygments
style_file_install=$(pygments_dir)/styles/$(style_file)
lexer_file_install=$(pygments_dir)/lexers/$(lexer_file)
dockerimage:=registry.gitlab.com/mark.e.royer/texlive-full-with-emacs:20210622
dockeropts:=-u 1000:1000 -w /test \
-v $(CURDIR):/test \
-v /etc/group:/etc/group:ro \
-v /etc/passwd:/etc/passwd:ro \
-it --rm

.PHONY: all clean\
        install-files\
        startdocker startdocker-build startdocker-dump-css\
        warnings

all: install-files $(name).pdf warnings

$(name).pdf: $(name).tex
	latexmk -g -pdf -shell-escape  $(name).tex

install-files: $(style_file_install) $(lexer_file_install)

warnings: $(name).pdf
	@grep Warning $(name).log;true

# Start docker and leave open
startdocker:
	docker run $(dockeropts) $(dockerimage)

# If you don't want to keep the container alive, build the default
# make target and exit.
startdocker-build:
	docker run $(dockeropts) $(dockerimage) make

startdocker-dump-css:
	docker run $(dockeropts) $(dockerimage)\
               bash -c\
               "make install-files && pygmentize -S eclipse -f html"

$(style_file_install): $(style_file)
	cp $(style_file) $(style_file_install)
	# The style file will be found automatically.
	# Make tex file dirty so that it is rebuilt
	touch $(name).tex && rm -rf _minted-$(name)

$(lexer_file_install): $(lexer_file)
	cp $(lexer_file) /usr/lib/python3/dist-packages/pygments/lexers/
	# We have to register the new lexer mapping.
	cd /usr/lib/python3/dist-packages/pygments/lexers/ &&\
	python3 _mapping.py
	# Make tex file dirty so that it is rebuilt
	touch $(name).tex && rm -rf _minted-$(name)


clean:
	rm -rf **/*~ **/*-eps-converted-to.pdf *-eps-converted-to.pdf \
               $(addprefix $(name),.aux .dvi .log .nav .out .pdf .snm .toc .vrb\
                          -pics.pdf -autopp.vrb -autopp.log -autopp.run.xml\
                          -blx.bib .bcf .run.xml .bbl .blg .html .url\
                           .fdb_latexmk fls .fdb_latexmk .fls)\
               auto _minted-$(name)
