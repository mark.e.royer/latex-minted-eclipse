#!/usr/bin/env python3
"""
    sudo cp eclipse.py /usr/lib/python3/dist-packages/pygments/styles/

   or

    cp  /usr/local/lib/python3.9/site-packages/pygments/styles/


"""

from pygments.style import Style
from pygments.token import (
    Keyword,
    Name,
    Comment,
    String,
    Error,
    Number,
    Operator,
    Generic,
    Whitespace,
)


class EclipseStyle(Style):
    """Standard Eclipse code style.  This class is loaded in

    pygments/styles/__init__.py

    by using the module name.  The loader assumes there is a class named

    module_name.title() + "Style"

    so the class name should "match" the module name.
    """

    string_color = "#2800ff"
    field_color = "#2800ff"
    annotation_color = "#636363"
    keyword_color = "#7f0054"
    comment_color = "#3f7f5e"
    javadoc_color = "#3f5eBf"
    jtag_color = "#7f9eBf"  # Javadoc tag color: @author, for example
    etag_color = "#7f9eBf"
    line_number_color = "#777777"

    default_style = ""

    styles = {
        Whitespace: "#ffffff",
        Comment: f"nobold {comment_color}",
        Comment.Multiline: comment_color,
        Comment.Special: f"bold {jtag_color}",
        Comment.Hashbang: f"bold {etag_color}",
        Comment.Preproc: annotation_color,
        Keyword: f"bold {keyword_color}",
        Keyword.Constant: f"bold {keyword_color}",
        Keyword.Type: f"bold {keyword_color}",  # primitive types
        Operator: "#000",  # e.g. +, -, (, )
        Number: "#000",
        Name.Class: "nobold #000",
        Name.Namespace: "nobold #000",
        Name.Exception: "nobold #000",
        Name.Entity: "nobold #000",
        Name.Function: "nobold #000",
        Name.Decorator: f"nobold {annotation_color}",
        String: string_color,
        String.Doc: javadoc_color,  # Javadoc sting
        # The following are common settings and are probably not required
        Generic.Heading: "bold",
        Generic.Subheading: "bold",
        Generic.Emph: "italic",
        Generic.Strong: "bold",
        Generic.Prompt: "bold",
        Error: "border:#f00",
    }


# cspell:ignore jtag Emph
