#!/usr/bin/env python3
""" Represent an extended java lexer for additional Javadoc support.
"""
import re

from pygments.filter import Filter
from pygments.filters import _replace_special
from pygments.lexer import RegexLexer, bygroups, using, this
from pygments.token import (
    Text,
    Comment,
    Operator,
    Keyword,
    Name,
    String,
    Number,
    Punctuation,
)
from pygments.util import get_list_opt


__all__ = ["JavaELexer"]  # This list of lexers is required. JavaELexer defined below.

# Pygment filters are defined in the pygments/filters/__init__.py file.
# We will define a few here that will be added to the JavaELexer defined below.


class HTMLTagFilter(Filter):
    """Highlight HTML tags in doc strings.

    Options accepted: None
    """

    def __init__(self, **options):
        Filter.__init__(self, **options)

        # Eclipse only matches HTML tags with no white space
        self.tag_re = re.compile(r"(<\S+?>|<\S+?/>)")

    def filter(self, lexer, stream):
        regex = self.tag_re
        for ttype, value in stream:
            if ttype in String.Doc:
                yield from _replace_special(ttype, value, regex, Comment.Preproc)
            else:
                yield ttype, value


class AttenTagFilter(Filter):
    """Highlight special code tags in comments and doc strings. This is
       basically the same as CodeTagFilter found in

       pygments/filters/__init__.py

       ,but that class does not take an option to change the style class
        applied, and we want it to be Comment.Hashbang, so...

    Options accepted:

    `codetags` : list of strings
       A list of strings that are flagged as code tags.  The default is to
       highlight ``XXX``, ``TODO``, ``BUG`` and ``NOTE``.
    """

    def __init__(self, **options):
        Filter.__init__(self, **options)
        tags = get_list_opt(options, "codetags", ["XXX", "TODO", "BUG", "NOTE"])
        self.tag_re = re.compile(
            r"\b(%s)\b" % "|".join([re.escape(tag) for tag in tags if tag])
        )

    def filter(self, lexer, stream):
        regex = self.tag_re
        for ttype, value in stream:
            if ttype in String.Doc or ttype in Comment and ttype not in Comment.Preproc:
                yield from _replace_special(ttype, value, regex, Comment.Hashbang)
            else:
                yield ttype, value


class DocTagFilter(Filter):
    """Highlight special code tags in comments and doc strings.

    Options accepted:

    `codetags` : list of strings
       A list of doc strings that are flagged as special.  The default is to
       highlight the following

    Here are the Javadoc tags and the version they were introduced based on the Java 7 docs here:

    http://docs.oracle.com/javase/7/docs/technotes/tools/solaris/javadoc.htmljavadoctags

    Currently does not handle the tags that are surrounded by brackets for example {@code}.

    TagName       Ver
    -----------------
    @author 	  1.0
    @deprecated   1.0
    @exception 	  1.0
    @param 	  1.0
    @return 	  1.0
    @see 	  1.0
    @serial 	  1.2
    @serialData   1.2
    @serialField  1.2
    @since	  1.1
    @throws 	  1.2
    @version	  1.0

    The following are not filtered.

    {@code} 	  1.5
    {@docRoot} 	  1.3
    {@inheritDoc} 1.4
    {@link} 	  1.2
    {@linkplain}  1.4
    {@literal} 	  1.5
    {@value} 	  1.4

    """

    default_jtags = [
        "@author",
        "@deprecated",
        "@exception",
        "@param",
        "@return",
        "@see",
        "@serial",
        "@serialData",
        "@serialField",
        "@since",
        "@throws",
        "@version",
    ]

    def __init__(self, **options):
        Filter.__init__(self, **options)
        tags = get_list_opt(options, "codetags", self.default_jtags)
        self.tag_re = re.compile(
            r"(%s)\b" % "|".join([re.escape(tag) for tag in tags if tag])
        )

    def filter(self, lexer, stream):
        regex = self.tag_re
        for ttype, value in stream:
            if ttype in String.Doc or ttype in Comment and ttype not in Comment.Preproc:
                # print(f"Found filter match {ttype} type and {value} value")
                yield from _replace_special(ttype, value, regex, Comment.Special)
            else:
                yield ttype, value


class JavaELexer(RegexLexer):
    """
    For `Java <https://www.oracle.com/technetwork/java/>`_ source code.
    """

    def __init__(self, **options):
        super().__init__(**options)

        # Not usually done, but we always want these filters used
        self.add_filter(DocTagFilter())
        self.add_filter(AttenTagFilter())
        self.add_filter(HTMLTagFilter())

    name = "JavaE"
    aliases = ["javae"]
    filenames = ["*.java"]
    mimetypes = ["text/x-java"]

    flags = re.MULTILINE | re.DOTALL | re.UNICODE

    tokens = {
        "root": [
            (r"[^\S\n]+", Text),
            (r"//.*?\n", Comment.Single),
            (r"/\*\*.*?\*/", String.Doc),  # Mark Javadoc as String.Doc
            (r"/\*.*?\*/", Comment.Multiline),
            # keywords: go before method names to avoid lexing "throw new XYZ"
            # as a method signature
            (
                r"(assert|break|case|catch|continue|default|do|else|finally|for|"
                r"if|goto|instanceof|new|return|switch|this|throw|try|while)\b",
                Keyword,
            ),
            # method names
            (
                r"((?:(?:[^\W\d]|\$)[\w.\[\]$<>]*\s+)+?)"  # return arguments
                r"((?:[^\W\d]|\$)[\w$]*)"  # method name
                r"(\s*)(\()",  # signature start
                bygroups(using(this), Name.Function, Text, Punctuation),
            ),
            (r"@[^\W\d][\w.]*", Name.Decorator),
            (
                r"(abstract|const|enum|extends|final|implements|native|private|"
                r"protected|public|static|strictfp|super|synchronized|throws|"
                r"transient|volatile)\b",
                Keyword.Declaration,
            ),
            (r"(boolean|byte|char|double|float|int|long|short|void)\b", Keyword.Type),
            (r"(package)(\s+)", bygroups(Keyword.Namespace, Text), "import"),
            (r"(true|false|null)\b", Keyword.Constant),
            (r"(class|interface)(\s+)", bygroups(Keyword.Declaration, Text), "class"),
            (r"(var)(\s+)", bygroups(Keyword.Declaration, Text), "var"),
            (
                r"(import(?:\s+static)?)(\s+)",
                bygroups(Keyword.Namespace, Text),
                "import",
            ),
            (r'"', String, "string"),
            (r"'\\.'|'[^\\]'|'\\u[0-9a-fA-F]{4}'", String.Char),
            (r"(\.)((?:[^\W\d]|\$)[\w$]*)", bygroups(Punctuation, Name.Attribute)),
            (r"^\s*([^\W\d]|\$)[\w$]*:", Name.Label),
            (r"([^\W\d]|\$)[\w$]*", Name),
            (
                r"([0-9][0-9_]*\.([0-9][0-9_]*)?|"
                r"\.[0-9][0-9_]*)"
                r"([eE][+\-]?[0-9][0-9_]*)?[fFdD]?|"
                r"[0-9][eE][+\-]?[0-9][0-9_]*[fFdD]?|"
                r"[0-9]([eE][+\-]?[0-9][0-9_]*)?[fFdD]|"
                r"0[xX]([0-9a-fA-F][0-9a-fA-F_]*\.?|"
                r"([0-9a-fA-F][0-9a-fA-F_]*)?\.[0-9a-fA-F][0-9a-fA-F_]*)"
                r"[pP][+\-]?[0-9][0-9_]*[fFdD]?",
                Number.Float,
            ),
            (r"0[xX][0-9a-fA-F][0-9a-fA-F_]*[lL]?", Number.Hex),
            (r"0[bB][01][01_]*[lL]?", Number.Bin),
            (r"0[0-7_]+[lL]?", Number.Oct),
            (r"0|[1-9][0-9_]*[lL]?", Number.Integer),
            (r"[~^*!%&\[\]<>|+=/?-]", Operator),
            (r"[{}();:.,]", Punctuation),
            (r"\n", Text),
        ],
        "class": [(r"([^\W\d]|\$)[\w$]*", Name.Class, "#pop")],
        "var": [(r"([^\W\d]|\$)[\w$]*", Name, "#pop")],
        "import": [(r"[\w.]+\*?", Name.Namespace, "#pop")],
        "string": [
            (r'[^\\"]+', String),
            (r"\\\\", String),  # Escaped backslash
            (r'\\"', String),  # Escaped quote
            (r"\\", String),  # Bare backslash
            (r'"', String, "#pop"),  # Closing quote
        ],
    }
